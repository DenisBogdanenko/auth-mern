import React, {useState} from 'react'
import './reg.scss'
import {registration} from "../../actions/user";

export const Reg = () => {
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')

    return (
        <div className='auth'>
            <div className="title"><h2>Registration</h2></div>
            <input
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                type='text'
                placeholder='email'
            />
            <input
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                type='password'
                placeholder='password'
            />
            <button className="auth-btn" onClick={() => registration(email, password)}>Зарегистрироваться</button>
        </div>
    )
}
