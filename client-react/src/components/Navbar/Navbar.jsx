import React from 'react'
import './navbar.scss'
import {NavLink} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {logout} from "../../reducers/userReducer";

export const Navbar = () => {
    const isAuth = useSelector(state => state.userReducer.isAuth)
    const dispatch = useDispatch()

    return (
        <div className='navbar'>
            <div className="container-nav">
                <div className="nav-title">
                    <h1>MERN-AUTH</h1>
                </div>
                <div className="header-nav">
                    {!isAuth && <NavLink activeClassName='nav-active' className='nav-btn' to="/" exact>Login</NavLink>}
                    {!isAuth && <NavLink activeClassName='nav-active' className='nav-btn' to="/registration">Registration</NavLink>}
                    {isAuth && <div onClick={() => dispatch(logout())} className='nav-btn'>Log out</div>}
                </div>
            </div>
        </div>
    )
}
