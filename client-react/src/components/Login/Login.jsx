import React, {useState} from 'react'
import './login.scss'
import {useDispatch} from "react-redux";
import {login} from "../../actions/user";

export const Login = () => {
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const dispatch = useDispatch()

    return (
        <div className='auth'>
            <div className="title"><h2>Login</h2></div>
            <input
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                type='text'
                placeholder='email'
            />
            <input
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                type='password'
                placeholder='password'
            />
            <button onClick={() => dispatch(login(email, password))} className="auth-btn">Войти</button>
        </div>
    )
}
