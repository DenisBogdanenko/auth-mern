import './app.scss'
import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {Navbar} from "./Navbar/Navbar";
import {BrowserRouter, Route, Switch} from "react-router-dom";
import {Login} from "./Login/Login";
import {Reg} from "./Reg/Reg";
import {auth} from "../actions/user";
import {Userpage} from "./Userpage/Userpage";

function App() {
    const isAuth = useSelector(state => state.userReducer.isAuth)
    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(auth())
    }, [])

    return (
        <BrowserRouter>
            <div className="App">
                <Navbar/>
                {!isAuth &&
                <Switch>
                    <Route path='/' component={Login} exact/>
                    <Route path='/registration' component={Reg}/>
                </Switch>
                }
                {isAuth && <Userpage/>}
            </div>
        </BrowserRouter>
    );
}

export default App;
