import React from "react";
import {useSelector} from "react-redux";

export const Userpage = () => {
    const user = useSelector(state => state.userReducer.user)

    return(
            <h1 style={{margin: '100px auto'}}>Hello, {user.email}</h1>
    )
}
