const express = require('express')
const router = new express.Router()
const {check} = require('express-validator')
const RegController = require('../controllers/RegController')
const LoginController = require('../controllers/LoginController')
const UserController = require('../controllers/UserController')
const authMiddleware = require('../middlewere/auth.middlewere')

router.get('/users', UserController.getAll)

router.post('/registration', [
    check('email', 'uncorrect email').isEmail(),
    check('password', 'uncorrect password').isLength({min: 3, max: 12})
], RegController.create)

router.post('/login', LoginController.login)

router.get('/auth', authMiddleware, UserController.auth)


module.exports = router
