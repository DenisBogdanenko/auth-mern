const express = require('express')
const mongoose = require('mongoose')
const config = require('config')
const cors = require('cors')
const authRouter = require('./routes/auth.routes.js')

const PORT = config.get("port")
const DB_URL = config.get("dbURL")

const app = express()
app.use(cors())
app.use(express.json())
app.use('/api/auth', authRouter)
app.get('/', (req, res) => {
    res.send('Hello')
})

async function start() {
    try {
        await mongoose.connect(DB_URL, {
            useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false, useCreateIndex: true
        });

        app.listen(PORT, () => console.log(`Server working on port `, PORT))
    } catch (e) {
        console.log(e)
    }
}

start()
