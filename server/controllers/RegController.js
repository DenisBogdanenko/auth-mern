const User = require('../models/User')
const {validationResult} = require('express-validator')
const bcrypt = require('bcrypt')

class RegController {
    async create(req, res) {
        try {
            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({message: 'uncorrect request', errors})
            }
            const {email, password} = req.body
            const candidate = await User.findOne({email})
            if (candidate) {
                return res.status(400).json({message: `User with email ${email} already exist`})
            }
            const hashedPassword = await bcrypt.hash(password, 8)
            const user = new User({email, password: hashedPassword})
            await user.save()
            return res.json({message: "user was created"})
        } catch (e) {
            console.log(e)
            res.send('server error')
        }
    }
}

module.exports = new RegController()
